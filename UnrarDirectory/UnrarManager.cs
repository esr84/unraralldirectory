﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;


namespace UnrarDirectory
{
    class UnrarManager
    {

        private BackgroundWorker _worker;
        private string _unrarPath;
        private string _input;
        private string _arguments;

        public delegate void EndProcessEvent();
        public EndProcessEvent endProcess = null;

        public delegate void ProgressProcessEvent(int numFileProcess);
        public ProgressProcessEvent progressProcess;

        public UnrarManager()
        {
            string applicationPath = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", "");
            applicationPath = applicationPath.Substring(0, applicationPath.LastIndexOf("/"));
            _unrarPath = applicationPath + "/UnRAR.exe";
        }

        static public int getNumRarFiles(string input)
        {
            return Directory.GetFiles(input, "*.rar").Length;
        }

        public void unrarFiles(string input, string output, string pwd)
        {
       
            if (!System.IO.File.Exists(_unrarPath))
                throw new Exception("Fallo al ejecutar el programa " + _unrarPath);

            _input = input;
            _arguments = " x -p" + pwd + " \"{0}\" \"" + output + "\"";
            //_unrarPath =  "\""+ _unrarPath + "\" x -p" + pwd + " \"{0}\" \"" + output + "\"";

            _worker = new BackgroundWorker();
            _worker.WorkerReportsProgress = true;
            _worker.DoWork += doWork;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.ProgressChanged += ProgressChanged;


            _worker.RunWorkerAsync();

           
        }

        private void doWork(object sender, DoWorkEventArgs e)
        {
            string unrarArguments;
            try
            {
                string []files = Directory.GetFiles(_input,"*.rar");

                for (int c = 0; c < files.Length; ++c)
                {
                    unrarArguments = string.Format(_arguments, files[c]);
                    unrarFile(_unrarPath, unrarArguments);
                    _worker.ReportProgress(c);
                }                
            }
            catch (Exception ex)
            {

            }
        }

        private void unrarFile(string unrarExecution, string arguments)
        {
            Process proc = new Process();
            ProcessStartInfo info = new ProcessStartInfo(unrarExecution, arguments);
            proc.StartInfo = info;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();

            proc.WaitForExit();
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (endProcess != null)
            {
                endProcess();
            }
        }

        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressProcess(e.ProgressPercentage);
        }
    }
}
