﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Forms;
using Microsoft.Win32;

namespace UnrarDirectory
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string inputDir = null;
        private string outputDir = null;

        private int numfiles;
        public MainWindow()
        {
            InitializeComponent();
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog openDirectory = new FolderBrowserDialog();
            if (openDirectory.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            lblInput.Content = inputDir = openDirectory.SelectedPath;

            numfiles = UnrarManager.getNumRarFiles(inputDir);

            lblStatus.Content = "Status: " + numfiles + " numero de ficheros rar a procesar";
        }

        private void cmdOutput_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog openDirectory = new FolderBrowserDialog();
            if (openDirectory.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            lblOutput.Content = outputDir = openDirectory.SelectedPath;
        }

        private void cmdUnrar_Click(object sender, RoutedEventArgs e)
        {
            if (inputDir == null || outputDir == null)
                return;

            UnrarManager manager = new UnrarManager();

            manager.endProcess = endUnrarProcess;
            manager.progressProcess = progressStatus;

            manager.unrarFiles(inputDir, outputDir, txtPwd.Text);
        }

        private void endUnrarProcess()
        {
            lblStatus.Content = "Status: Completed";
        }

        private void progressStatus(int processedFiles)
        {
            lblStatus.Content = "Status: " + processedFiles + " de " + numfiles;
        }
    }
}
